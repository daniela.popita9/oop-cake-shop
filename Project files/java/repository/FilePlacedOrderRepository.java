package company.repository;

import company.domain.*;
import company.exceptions.*;

import java.io.*;
import java.util.Properties;

public class FilePlacedOrderRepository extends AbstractRepository<PlacedOrder, Integer>{
	
	public FileCakeRepository cakes_available;
	private String filename;

    public FilePlacedOrderRepository(String filename, FileCakeRepository cakesRepo) {
        this.filename = filename;
        this.cakes_available = cakesRepo;
        readFromFile();
    }

	public FilePlacedOrderRepository( FileCakeRepository cakesRepo) {
		SetFilenameFromProperties();
		this.cakes_available = cakesRepo;
		readFromFile();
	}


	public void setCakesAvailable(FileCakeRepository cakesAvailable)
	{
		this.cakes_available=cakesAvailable;
	}

	private void SetFilenameFromProperties(){
		try {
			Properties properties = new Properties();
			properties.load(new FileInputStream("CakeOrdersApp.properties"));//"C:\\Daniela\\scoala\\anul 2\\semestrul 1\\MAP\\Labs\\FileJavaFXC&O\\src\\main\\java\\CakeOrdersApp.properties"));
			this.filename=properties.getProperty("OrdersFile").trim();
			if (this.filename==null){
				this.filename= "src/orders.txt";
				System.err.println("Cakes file not found. Using default"+this.filename);
			}

		}catch (IOException ex){
			System.err.println("Error reading the configuration file"+ex);
		}

	}


    private void readFromFile() {
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) 
        {
            String line;
            while ((line = reader.readLine()) != null) 
            {
                String[] el = line.split(";");
                if (el.length != 7) 
                {
                	System.err.println("Not a valid number of attributes" + line);
                    continue;
                }

                    int Id = Integer.parseInt(el[0]);
					Boolean finished = Boolean.parseBoolean(el[5]);
					System.out.println(finished);
					PlacedOrder p = new PlacedOrder(Id, el[1], el[2], el[3], cakes_available.findById(Integer.parseInt(el[4])), finished, el[6]);
                    PlacedOrder.setNextId(PlacedOrder.getNextId()+1);
                    super.add(p);
            }
        } catch (IOException | AlreadyInRepoException | NotFoundException| NumberFormatException ex)
        	{
            	throw new FailFileOperationException("Error reading" + ex);
        	}
    }

    @Override
    public void add(PlacedOrder obj) throws NotFoundException, AlreadyInRepoException {
        try {
    		if(!cakes_available.GetRepo().contains(obj.getCakeType()))
    			throw new NotFoundException("There is no " + obj.getCakeType().toStringNotid() +" in the cake list");
            super.add(obj);
            writeToFile();
        } catch (RuntimeException e) 
        	{
            	throw new FailFileOperationException("Object was not added" + e + " "+obj);
        	}
	}

    private void writeToFile() {
        try (PrintWriter pw = new PrintWriter(filename)) {
            for (PlacedOrder el : findAll()) {
                String line = el.getId() + ";"+el.getClientName()+";"+el.getClientAddress() + ";"+el.getDueDate() +";" + el.getCakeType().getId() +
                		";"+el.getIsFinished() +";"+el.getDecorations();
              	pw.println(line);
            }
        } catch (IOException ex)
        	{
            	throw new FailFileOperationException("Error writing" + ex);
        	}
    }

    @Override
    public void deleteByContent(PlacedOrder obj) throws NotFoundException {
        try {
            super.deleteByContent(obj);
            writeToFile();
        } catch (RuntimeException ex) 
        	{
            	throw new FailFileOperationException("Object was not deleted" + ex +" "+obj);
        	}
    }

	public void deleteById(Integer id) throws NotFoundException{
		try {
			super.deleteById(id);
			writeToFile();
		}
		catch (RuntimeException ex)
		{
			throw new FailFileOperationException("Object of id :"  + id + "was not deleted" + ex);
		}
	}


	@Override
    public void update(PlacedOrder obj, Integer id) throws NotFoundException, AlreadyInRepoException{
        try 
        {
    		if(!cakes_available.GetRepo().contains(obj.getCakeType()))
    			throw new NotFoundException("There is no " + obj.getCakeType().toStringNotid() +" in the cake list");

        	super.update(obj, id);
            writeToFile();
        } catch (RuntimeException ex)
        	{
            	throw new FailFileOperationException("Object was not updated" + ex + " "+obj);
        	}
    }
    
	public void FinishOrder(Integer orderId) throws NotFoundException 
	{
		super.findById(orderId).setIsFinished(true);
		writeToFile();
	}
}