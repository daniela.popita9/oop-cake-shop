package company.repository;

import company.domain.*;
import company.exceptions.*;

import java.io.*;
import java.util.Properties;


public class FileCakeRepository extends AbstractRepository<Cake, Integer>{

	private String BasicFileName;

	public FileCakeRepository(String BfileName)
	{
		this.BasicFileName =BfileName;
		try{
			this.ReadFromFile();
		}catch (FailFileOperationException n)
		{
			System.err.println(" Error " + n.getMessage());
			System.out.println();
		}
	}

	public FileCakeRepository() {
		SetFilenameFromProperties();
		ReadFromFile();
	};

	private void SetFilenameFromProperties(){
		try {
			Properties properties = new Properties();
			properties.load(new FileInputStream("CakeOrdersApp.properties"));//"C:\\Daniela\\scoala\\anul 2\\semestrul 1\\MAP\\Labs\\FileJavaFXC&O\\src\\main\\java\\CakeOrdersApp.properties"));
			this.BasicFileName=properties.getProperty("CakesFile");
			if (this.BasicFileName==null){
				this.BasicFileName = "src/cakes.txt";
				System.err.println("Cakes file not found. Using default "+this.BasicFileName);
			}

		}catch (IOException ex){
			System.err.println("Error reading the configuration file"+ex);
		}

	}

	public void ReadFromFile() 
	{
		try(BufferedReader reader= new BufferedReader(new FileReader(this.BasicFileName)))
		{
			String line;
			while((line=reader.readLine())!=null)
			{
				String[] element = line.split(";");
				if(element.length!=7)
					{
						System.err.println("Not a valid number of attributes "+line);
						continue;
					}
				Integer Id=Integer.parseInt(element[0]);
				Integer number_of_layers=Integer.parseInt(element[5]);
				Boolean GlutenFree=Boolean.parseBoolean(element[6]);
				Cake c= new Cake(Id,element[1],element[2], element[3], element[4], number_of_layers, GlutenFree);
				Cake.setNextId(Cake.getNextId()+1);
				super.add(c);
			}
		}
		catch(IOException | AlreadyInRepoException | NotFoundException ex)
			{
				throw new FailFileOperationException("Error reading "+ex.getMessage());
			}

	}
	
	private void writeToTXTFile() {
		try(PrintWriter pw = new PrintWriter(this.BasicFileName)) {
			for(Cake cake : findAll()) 
			{
				String line = cake.getId()+ ";" + cake.getName() + ";" + cake.getFlavor() + ";" + cake.getFilling() + ";" 
				+ cake.getIcing() +";"+ cake.getNumber_of_layers() + ";" + cake.getGlutenFree();
				
				pw.println(line);
			}
		}
		catch(IOException ex) 
		{
			throw new FailFileOperationException("Error writing" + ex.getMessage());
		}
	}

	@Override
	public void add(Cake obj) throws AlreadyInRepoException, NotFoundException, FailFileOperationException {
		try{
			super.add(obj);
			writeToTXTFile();
			//writeToSerializationFile();
		}
		catch(RuntimeException e)
		{
			throw new FailFileOperationException("Object wasn't added" + e + " " + obj);
		}
	}
	
	@Override
	public void deleteByContent(Cake obj) throws NotFoundException{
		try{
			super.deleteByContent(obj);
			writeToTXTFile();
			//writeToSerializationFile();

		}
		catch(RuntimeException ex)
		{
			throw new FailFileOperationException("Object was not deleted"+ex+" "+obj);
		}
	}
	
	@Override
	public void deleteById(Integer id) throws NotFoundException{
		try{
			super.deleteById(id);
			writeToTXTFile();
			//writeToSerializationFile();

		}
		catch(RuntimeException ex)
		{
			throw new FailFileOperationException("Object was not deleted"+ex+" there is no object of id" + id +"in the cake list" );
		}
	}
	
	@Override
	public void update(Cake obj, Integer id) throws NotFoundException, AlreadyInRepoException, FailFileOperationException{
		try{

			super.update(obj, id);
			writeToTXTFile();
			//writeToSerializationFile();

		}
		catch(RuntimeException ex)
		{
			throw new FailFileOperationException("Object was not updated" + ex + " " + obj);
		}
	}

	
}
