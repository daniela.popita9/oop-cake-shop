package company.repository;

import company.repository.InterfaceRepository.Repository;
import company.domain.InterfaceDomain.Identifiable;
import company.exceptions.*;

import java.util.ArrayList;
//import exceptions.*;

public abstract class AbstractRepository <T extends Identifiable<ID>,ID> implements Repository<T, ID> {

    protected ArrayList<T> AbstractRepo;
    public ArrayList<T> GetRepo()
    {
        return this.AbstractRepo;
    }

    public void SetRepo(ArrayList<T> repo)
    {
        this.AbstractRepo=repo;
    }

    public AbstractRepository()
    {
        this.AbstractRepo = new ArrayList<>();
    }

    @Override
    public void add(T elem) throws AlreadyInRepoException, NotFoundException
    {
        if(this.AbstractRepo.contains(elem))
            throw new AlreadyInRepoException("The " + elem.toStringNotid() +" is already in the list");

        this.AbstractRepo.add(elem);
    }

    @Override
    public void deleteByContent(T elem) throws NotFoundException {
        // Deletes an object by its content
        boolean found=false;
        for (T c:this.AbstractRepo)
        {
            if (c.equals(elem))
            {
                this.AbstractRepo.remove(c);
                found=true;
                break;
            }
        }
        if (!found)
        {
            throw new NotFoundException("The " + elem.toStringNotid() +" is not in the list");
        }

    }

    public void deleteById(ID id) throws NotFoundException {
        //Deletes an object by its ID
        boolean found=false;
        for (T c:this.AbstractRepo)
        {

            if (c.getId().equals(id))
            {
                this.AbstractRepo.remove(c);
                found=true;
                break;
            }
        }
        if (!found)
        {
            throw new NotFoundException("The elem of id " + id +" is not in the list");
        }

    }


    @Override
    public void update(T elem, ID id) throws NotFoundException, AlreadyInRepoException {
        boolean found=false;
        for (T c:this.AbstractRepo)
        {
            if (c.equals(elem))
            {
                throw new AlreadyInRepoException(" The cake with this content already exists " + elem.toStringNotid() +" in the list");
            }
            if (c.getId().equals(id))
            {
                found=true;
                elem.setId(c.getId());
                this.AbstractRepo.set(this.AbstractRepo.indexOf(c), elem);
                break;
            }
        }
        if (!found)
        {
            throw new NotFoundException("There is no element of id " + id +" in the list");
        }


    }

    @Override
    public T findById(ID id) throws NotFoundException {
        boolean found=false;
        for (T c:this.AbstractRepo)
        {
            if (c.getId().equals(id))
            {
                return c;
            }
        }
        if (!found)
        {
            throw new NotFoundException("There is no element of id " + id +" in the list");
        }
        return null;

    }

    @Override
    public Iterable<T> findAll() {
        return this.AbstractRepo;
    }

}

