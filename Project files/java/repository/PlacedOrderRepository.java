package company.repository;

import company.domain.*;
import company.exceptions.*;

import java.util.ArrayList;

public class PlacedOrderRepository extends AbstractRepository<PlacedOrder, Integer>
{
	public FileCakeRepository cakes_available;
	
	public PlacedOrderRepository(){}
	
	public void setCakesAvailable(FileCakeRepository cakesAvailable)
	
	{
		this.cakes_available=cakesAvailable;
	}
	
	@Override
	public void add(PlacedOrder elem) throws AlreadyInRepoException, NotFoundException 
	{
		
		if(!cakes_available.GetRepo().contains(elem.getCakeType()))
			throw new NotFoundException("There is no " + elem.getCakeType().toStringNotid() +" in the cake list");
		super.add(elem);
	}
	
	@Override
	public void update(PlacedOrder elem, Integer id) throws AlreadyInRepoException, NotFoundException 
	{
		
		if(!cakes_available.GetRepo().contains(elem.getCakeType()))
			throw new NotFoundException("There is no " + elem.getCakeType().toStringNotid() +" in the cake list");
		super.update(elem, id);
	}

	public void FinishOrder(Integer orderId) throws NotFoundException 
	{
		try 
		{
			super.findById(orderId).setIsFinished(true);
		}
		catch (NotFoundException n)
		{
			throw new NotFoundException("There is no order of id " + orderId +" in the list");
 
		}
		
	}

	public ArrayList<PlacedOrder> getFinishedOrders()
	{
	
		ArrayList<PlacedOrder> finishedOrderRepo= new ArrayList<>();
		for(PlacedOrder p :super.GetRepo() )			
		{
			if(p.getIsFinished())
				finishedOrderRepo.add(p);
		}
		return finishedOrderRepo;
	}
		
	public ArrayList<PlacedOrder> getOrdersFromCity(String city) 
		{
			
		
		ArrayList<PlacedOrder> OrderFromCityRepo= new ArrayList<>();
			for(PlacedOrder p :super.GetRepo() )
			{
				
				if(p.getClientAddress().equals(city))
					OrderFromCityRepo.add(p);
			}
			return OrderFromCityRepo;
		
	}

	public ArrayList<PlacedOrder> getOrdersForClient(String client)
	{

		ArrayList<PlacedOrder> OrderForClientRepo= new ArrayList<>();
		for(PlacedOrder p :super.GetRepo() )
		{
			
			if(p.getClientName().equals(client))
				OrderForClientRepo.add(p);
		}
		return OrderForClientRepo;
	
	}

}