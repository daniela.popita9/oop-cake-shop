package company.exceptions;

public class AlreadyInRepoException extends Exception{
	
	private static final long serialVersionUID = 4144051276554308673L;
	public AlreadyInRepoException(){}
	public AlreadyInRepoException(String message)
	{
		super(message);
	}
}