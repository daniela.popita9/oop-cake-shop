package company.exceptions;

public class FailFileOperationException extends RuntimeException{

	private static final long serialVersionUID = -4043954085327408804L;

	public FailFileOperationException(){}
	
	public FailFileOperationException(String message){
		super(message);
	}
	
}
