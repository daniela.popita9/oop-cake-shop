package company.Service.InterfaceController;

import company.exceptions.*;

import java.util.Spliterator;

public interface Controller <T,Tid>
{
	void add(T elem) throws AlreadyInRepoException, NotFoundException;
	void deleteById(Tid id) throws NotFoundException;
	void deleteByContent(T elem) throws NotFoundException;
	void update (T elem, Tid id) throws NotFoundException, AlreadyInRepoException;
	T findById (Tid id) throws NotFoundException;
	Iterable<T> findAll();
	Spliterator<T> forStream();
}


