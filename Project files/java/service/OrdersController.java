package company.Service;

import company.Service.InterfaceController.Controller;
import company.domain.*;
import company.exceptions.*;
import company.repository.*;

import java.util.List;
import java.util.Spliterator;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class OrdersController implements Controller<PlacedOrder, Integer> {

	public FilePlacedOrderRepository ordersRepo;
	
	public OrdersController( FilePlacedOrderRepository newordersRepo)
	{
		this.ordersRepo=newordersRepo;
	}

/*	public OrdersController()
	{
		this.ordersRepo=new FilePlacedOrderRepository("C:\\Daniela\\scoala\\anul 2\\semestrul 1\\MAP\\Labs\\FileJavaFXC&O\\src\\main\\java\\orders.txt");
	}*/

	public OrdersController( CakeController cakeController)
	{
		this.ordersRepo = new FilePlacedOrderRepository(/*"C:\\Daniela\\scoala\\anul 2\\semestrul 1\\MAP\\Labs\\FileJavaFXC&O\\src\\main\\java\\orders.txt",*/ cakeController.get_cake_repo());
		this.ordersRepo.setCakesAvailable(cakeController.get_cake_repo());
	}


	@Override
	public void add(PlacedOrder elem) throws AlreadyInRepoException, NotFoundException {
		this.ordersRepo.add(elem);	
	}

	@Override
	public void deleteById(Integer id) throws NotFoundException {
		this.ordersRepo.deleteById(id);
	}

	@Override
	public void deleteByContent(PlacedOrder elem) throws NotFoundException {
		this.ordersRepo.deleteByContent(elem);
	}

	@Override
	public void update(PlacedOrder elem, Integer id) throws NotFoundException, AlreadyInRepoException {
		this.ordersRepo.update(elem, id);	
	}

	@Override
	public PlacedOrder findById(Integer id) throws NotFoundException {
		return this.ordersRepo.findById(id);
	}

	@Override
	public Iterable<PlacedOrder> findAll() {
		return this.ordersRepo.GetRepo();
	}

	@Override
	public Spliterator<PlacedOrder> forStream() {
		return this.ordersRepo.findAll().spliterator();
	}
	
	public void FinishOrder(Integer orderId) throws NotFoundException 
	{
		ordersRepo.FinishOrder(orderId);
	}
	
	
	public List<PlacedOrder> streamOrdersForCake(Integer id)
	{
		List<PlacedOrder> ordersForCake = StreamSupport.stream(this.forStream(), false).filter(PlacedOrder -> PlacedOrder.getCakeId().equals(id)).collect(Collectors.toList());
		return ordersForCake;
	}

	public List<PlacedOrder> streamFinishedOrders()
	{
		List<PlacedOrder> FinishedOrders= StreamSupport.stream(this.forStream(), false).filter(PlacedOrder -> PlacedOrder.getIsFinished()).collect(Collectors.toList());
		return FinishedOrders;
	}

	public List<PlacedOrder> streamNotFinishedOrders()
	{	
		List<PlacedOrder> notFinishedOrders= StreamSupport.stream(this.forStream(), false).filter(PlacedOrder -> !PlacedOrder.getIsFinished()).collect(Collectors.toList());
		return notFinishedOrders;
	}
	
	public List<Cake> streamCakesForDate(String date)
	{
		List<Cake> CakesForDate = StreamSupport.stream(this.forStream(), false).filter(p -> p.getDueDate().contains(date)).map(placedOrder -> placedOrder.getCakeType()).collect(Collectors.toList());
		return CakesForDate;
	}

}
