package company.Service;

import company.Service.InterfaceController.Controller;
import company.domain.*;
import company.exceptions.*;
import company.repository.*;

import java.util.List;
import java.util.Spliterator;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class CakeController implements Controller<Cake, Integer> {

	public FileCakeRepository cakeRepo;


	public CakeController( FileCakeRepository newCakeRepo)
	{
		this.cakeRepo=newCakeRepo;
	}
	
	public CakeController( )
	{
			this.cakeRepo = new FileCakeRepository();//("C:\\Daniela\\scoala\\anul 2\\semestrul 1\\MAP\\Labs\\FileJavaFXC&O\\src\\main\\java\\cakes.txt"); new FileCakeRepository();
	}
	public FileCakeRepository get_cake_repo()
	{
		return this.cakeRepo;
	}

	@Override
	public void add(Cake elem) throws AlreadyInRepoException, NotFoundException {
		this.cakeRepo.add(elem);
		Cake.setNextId(Cake.getNextId()+1);
	}

	@Override
	public void deleteById(Integer id) throws NotFoundException {
		this.cakeRepo.deleteById(id);
	}

	@Override
	public void deleteByContent(Cake elem) throws NotFoundException {
		this.cakeRepo.deleteByContent(elem);
	}

	@Override
	public void update(Cake elem, Integer id) throws NotFoundException, AlreadyInRepoException {
		this.cakeRepo.update(elem, id);	
	}

	@Override
	public Cake findById(Integer id) throws NotFoundException {
		return this.cakeRepo.findById(id);
	}

	@Override
	public Iterable<Cake> findAll() {
		return this.cakeRepo.GetRepo();
	}

	@Override
	public Spliterator<Cake> forStream() {
		return this.cakeRepo.findAll().spliterator();
	}
	
	public List<Integer> streamCakeWithNumberOfLayers(Integer layers)
	{
		List<Integer> CakeWithNumberOfLayersRepo = StreamSupport.stream(this.forStream(), false).filter(cake -> cake.getNumber_of_layers().equals(layers)).map(cake -> cake.getId()).collect(Collectors.toList());
		return CakeWithNumberOfLayersRepo;
	}

	public List<Cake> streamGlutenFreeCakes()
	{	
		List<Cake> getGlutenFreeCakeRepo= StreamSupport.stream(this.forStream(), false).filter(cake -> cake.getGlutenFree()).collect(Collectors.toList());
		return getGlutenFreeCakeRepo;
	}

	public List<Cake> streamNotGlutenFreeCakes()
	{
		List<Cake> getNotGlutenFreeCakeRepo= StreamSupport.stream(this.forStream(), false).filter(cake -> !cake.getGlutenFree()).collect(Collectors.toList());
		return getNotGlutenFreeCakeRepo;
	}

	public Long streamNumberOfCakesThatHaveFlavor(String flavor)
	{
		return( StreamSupport.stream(this.forStream(), false).filter(cake -> cake.getFlavor().contains(flavor)).count());
	}


}
