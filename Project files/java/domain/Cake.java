package company.domain;

import company.domain.InterfaceDomain.Identifiable;

import java.io.Serializable;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class Cake implements Identifiable<Integer>, Serializable
{
    private static Integer nextId=0;
    private Integer Id;
    private final SimpleStringProperty name = new SimpleStringProperty("");
    private final SimpleStringProperty flavor = new SimpleStringProperty("");
    private final SimpleStringProperty filling = new SimpleStringProperty("");
    private final SimpleStringProperty icing = new SimpleStringProperty("");
    private final SimpleIntegerProperty number_of_layers = new SimpleIntegerProperty( 0);
    private Boolean GlutenFree=false;

    public Cake(Integer id, String name, String flavor, String filling,	String icing, Integer number_of_layers,	Boolean  GlutenFree)
    {
        this.Id=id;
        this.name.setValue(name);
        this.flavor.setValue(flavor);
        this.filling.setValue(filling);
        this.icing.setValue(icing);
        this.number_of_layers.setValue(number_of_layers);
        this.GlutenFree = GlutenFree;
    }


    public Cake(String name, String flavor, String filling,	String icing, Integer number_of_layers,	Boolean  GlutenFree)
    {
        this.Id=nextId;
        //nextId++;
        this.name.setValue(name);
        this.flavor.setValue(flavor);
        this.filling.setValue(filling);
        this.icing.setValue(icing);
        this.number_of_layers.setValue(number_of_layers);
        this.GlutenFree=GlutenFree;
    }

    public Cake()
    {
        this.Id=nextId;
        //nextId++;
        this.name.setValue("");
        this.flavor.setValue("");
        this.filling.setValue("");
        this.icing.setValue("");
        this.number_of_layers.setValue(0);
        this.GlutenFree=false;
    }

    public static Integer getNextId() {
        return nextId;
    }

    public static void setNextId(Integer nextId) {
        Cake.nextId = nextId;
    }

    @Override
    public Integer getId() {
        return Id;
    }

    @Override
    public void setId(Integer id) {
        Id = id;
    }

    public String getName() {
        return name.getValue();
    }

    public void setName(String name) {
        this.name.setValue(name);
    }

    public String getFlavor() {
        return flavor.getValue();
    }

    public void setFlavor(String flavor) {
        this.flavor.setValue(flavor);
    }

    public String getFilling() {
        return filling.getValue();
    }

    public void setFilling(String filling) {
        this.filling.setValue(filling);
    }

    public String getIcing() {
        return icing.getValue();
    }

    public void setIcing(String icing) {
        this.icing.setValue(icing);
    }

    public Integer getNumber_of_layers() {
        return this.number_of_layers.getValue();
    }

    public void setNumber_of_layers(Integer number_of_layers) {
        this.number_of_layers.setValue(number_of_layers);
    }

    public Boolean getGlutenFree() {
        return GlutenFree;
    }

    public void setGlutenFree(Boolean glutenFree) {
        this.GlutenFree=glutenFree;
    }

    @Override
    public String toString()
    {
        String c="";
        c = c + "Cake of id " + this.Id;
        c = c +	" named " +	this.name.getValue();
        c = c + " has flavor of " + this.flavor.getValue();
        c = c + ", " + this.filling.getValue() + " filling";
        c = c + ", " + this.icing.getValue() + " icing, ";
        c = c + this.number_of_layers.getValue() + " layers";
        if(this.GlutenFree)
            c = c + " and is Gluten Free";
        return c;
    }

    public String toStringNotid()
    {
        String c="";
        c = c + "Cake named " +	this.name.getValue();
        c = c + " has flavor of " + this.flavor.getValue();
        c = c + ", " + this.filling.getValue() + " filling";
        c = c + ", " + this.icing.getValue() + " icing, ";
        c = c + this.number_of_layers.getValue() + " layers";
        if(this.GlutenFree)
            c = c + " and is Gluten Free";
        return c;
    }

    public boolean equals(Object ob){
        if(ob instanceof Cake)
        {
            Cake cake = (Cake)ob;
            return cake.name.getValue().equalsIgnoreCase(this.name.getValue()) && cake.flavor.getValue().equals(this.flavor.getValue()) && cake.filling.getValue().equals(this.filling.getValue()) && cake.icing.getValue().equals(this.icing.getValue())
                    && Boolean.compare(cake.GlutenFree, this.GlutenFree) == 0 && this.number_of_layers.getValue() == (int) cake.number_of_layers.getValue();

        }
        return false;
    }

 }

