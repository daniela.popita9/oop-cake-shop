package company.domain.InterfaceDomain;

public interface Identifiable<Tid>
{
    Tid getId();

    void setId(Tid var1);

    String toStringNotid();
}
