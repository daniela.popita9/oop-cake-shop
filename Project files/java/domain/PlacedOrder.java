package company.domain;

import company.domain.InterfaceDomain.Identifiable;
import java.io.Serializable;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.SimpleBooleanProperty;

public class PlacedOrder implements Identifiable<Integer>, Serializable
{
    private static Integer nextId=0;

    private Integer Id;
    private final SimpleStringProperty clientName = new SimpleStringProperty("");
    private final SimpleStringProperty clientAddress = new SimpleStringProperty("");
    private final SimpleStringProperty dueDate = new SimpleStringProperty("");
    private Cake cakeType;
    private final SimpleBooleanProperty isFinished = new SimpleBooleanProperty(false);
    private final SimpleStringProperty decorations = new SimpleStringProperty("");

    public PlacedOrder(String ClientName, String ClientAddress, String dueDate, Cake cakeType, String decorations)
    {
        this.Id=nextId;
        nextId++;
        this.clientName.setValue(ClientName);
        this.clientAddress.setValue(ClientAddress);
        this.dueDate.setValue(dueDate);
        this.cakeType= cakeType;
        this.decorations.setValue(decorations);
    }
    public PlacedOrder(Integer id, String ClientName, String ClientAddress, String dueDate, Cake cakeType, String decorations)
    {
        this.Id=id;
        this.clientName.setValue(ClientName);
        this.clientAddress.setValue(ClientAddress);
        this.dueDate.setValue(dueDate);
        this.cakeType= cakeType;
        this.decorations.setValue(decorations);
    }
    public PlacedOrder(Integer id, String ClientName, String ClientAddress, String dueDate, Cake cakeType, Boolean ISFinished, String decorations)
    {
        this.Id=id;
        this.clientName.setValue(ClientName);
        this.clientAddress.setValue(ClientAddress);
        this.dueDate.setValue(dueDate);
        this.cakeType= cakeType;
        this.decorations.setValue(decorations);
        this.cakeType= cakeType;
        this.isFinished.setValue(ISFinished);
    }
    public PlacedOrder()
    {
        this.Id=nextId;
        nextId++;
        this.clientName.setValue("");
        this.clientAddress.setValue("");
        this.dueDate.setValue("");
        this.cakeType=new Cake();
        this.decorations.setValue("");
    }


    public String getDueDate() {
        return dueDate.getValue();
    }

    public void setDueDate(String dueDate) {
        this.dueDate.setValue(dueDate);
    }


    public static Integer getNextId() {
        return nextId;
    }

    public static void setNextId(Integer nextId) {
        PlacedOrder.nextId = nextId;
    }


    @Override
    public Integer getId() {
        return Id;
    }

    @Override
    public void setId(Integer id) {
        Id = id;
    }


    public String getClientName() {
        return clientName.getValue();
    }

    public void setClientName(String clientName) {
        this.clientName.setValue(clientName);
    }


    public String getClientAddress() {
        return clientAddress.getValue();
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress.setValue(clientAddress);
    }


    public Cake getCakeType() {
        return cakeType;
    }

    public Integer getCakeId() {return cakeType.getId();}


    public void setCakeType(Cake cakeType) {
        this.cakeType = cakeType;
    }

    //used for printing the orders in javaFx interface
    public String getCakeNameAndID() {

        return cakeType.getName()+" of ID: "+cakeType.getId();
    }


    public Boolean getIsFinished() {
        return this.isFinished.getValue();
    }

    public void setIsFinished(Boolean isFinished) {
        this.isFinished.setValue(isFinished);
    }


    public String getDecorations() {
        return decorations.getValue();
    }

    public void setDecorations(String decorations) {
        this.decorations.setValue(decorations);
    }


    @Override
    public String toString()
    {
        String s;
        s = "Order of ID: " + this.Id;
        s = s + ", due in: "+this.dueDate.getValue();
        if(this.isFinished.getValue())
            s=s+" IS FINISHED     ";
        else
            s=s+" IS NOT FINISHED ";
        s = s + ", for client named: "+this.clientName.getValue();
        s = s + ", address:"+this.clientAddress.getValue();
        s = s + ", cake type: "+this.cakeType.getName();
        s=s+", with decorations: "+this.decorations.getValue();
        return s;
    }

    @Override
    public String toStringNotid() {
        String s;
        s = "Order due in: "+this.dueDate.getValue();
        if(this.isFinished.getValue())
            s=s+" IS FINISHED    ";
        else
            s=s+" IS NOT FINISHED ";
        s = s + ", for client named: "+this.clientName.getValue();
        s = s + ", address: "+this.clientAddress.getValue();
        s = s + ", cake type: "+this.cakeType.getName();
        s=s+", with decorations: "+this.decorations.getValue();
        return s;

    }

    public boolean equals(Object ob){
        if(ob instanceof PlacedOrder)
        {
            PlacedOrder placedOrder = (PlacedOrder)ob;
            return placedOrder.getClientAddress().equals(this.clientAddress.getValue()) && placedOrder.getClientName().equals(this.clientName.getValue()) && placedOrder.getDecorations().equals(this.decorations.getValue()) && placedOrder.getDueDate().equals(this.dueDate.getValue())
                    && Boolean.compare(placedOrder.getIsFinished(), this.isFinished.getValue()) == 0;
        }
        return false;
    }

}