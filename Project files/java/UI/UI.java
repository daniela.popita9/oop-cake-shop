package company.UI;

import company.Service.*;
import company.domain.*;
import company.exceptions.*;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class UI implements Initializable {

	private final CakeController cakeController;
	private final ObservableList<Cake> cake_list;

	private final OrdersController ordersController;
	private final ObservableList<PlacedOrder> orders_list;

	private final ObservableList<PlacedOrder> FilteredOrders_list;
	private final ObservableList<Cake> FilteredCakes_list;


	public UI() {
		this.cakeController = new CakeController();
		this.cake_list = FXCollections.observableArrayList();

		this.ordersController = new OrdersController(cakeController);
		this.orders_list = FXCollections.observableArrayList();

		this.FilteredOrders_list = FXCollections.observableArrayList();
		this.FilteredCakes_list = FXCollections.observableArrayList();

	}
 // the fields from add cake part
	@FXML private TextField name_tf;
	@FXML private TextField flavour_tf;
	@FXML private TextField filling_tf;
	@FXML private TextField nbLayers_tf;
	@FXML private TextField icing_tf;
	@FXML private CheckBox glutenFree_tf;
	@FXML private TextField id_tf;

	//the error text:
	@FXML private Text ErrorCake_tf;

//the fields from cake view part in CAKES
	@FXML private TableView<Cake> CakeTableView;
	@FXML private TableColumn<Cake,Integer> cakeIDcol;
	@FXML private TableColumn<Cake,String> cakeNAMEcol;
	@FXML private TableColumn<Cake,String> cakeFLAVOURcol;
	@FXML private TableColumn<Cake,String> cakeFILLINGcol;
	@FXML private TableColumn<Cake,String> cakeICINGcol;
	@FXML private TableColumn<Cake,Integer> cakeLAYERScol;
	@FXML private TableColumn<Cake,String> cakeGFreecol;

	//the fields from cake view part in CURRENT ORDERS
	@FXML private TableView<Cake> CakeTableView2;
	@FXML private TableColumn<Cake,Integer> cakeIDcol2;
	@FXML private TableColumn<Cake,String> cakeNAMEcol2;
	@FXML private TableColumn<Cake,String> cakeFLAVOURcol2;
	@FXML private TableColumn<Cake,String> cakeFILLINGcol2;
	@FXML private TableColumn<Cake,String> cakeICINGcol2;
	@FXML private TableColumn<Cake,Integer> cakeLAYERScol2;
	@FXML private TableColumn<Cake,String> cakeGFreecol2;

	//the fields from CAKE VIEW in REPORTS
	@FXML private TableView<Cake> ReportCakeTableView;
	@FXML private TableColumn<Cake,Integer> RcakeIDcol;
	@FXML private TableColumn<Cake,String> RcakeNAMEcol;
	@FXML private TableColumn<Cake,String> RcakeFLAVOURcol;
	@FXML private TableColumn<Cake,String> RcakeFILLINGcol;
	@FXML private TableColumn<Cake,String> RcakeICINGcol;
	@FXML private TableColumn<Cake,Integer> RcakeLAYERScol;
	@FXML private TableColumn<Cake,String> RcakeGFreecol;


	//the fields in REPORT
	@FXML private TextField ReportCake_id_tf;
	@FXML private Text ReportsError_tf;
	@FXML private DatePicker ReportDate_id_tf;



	@FXML public void resetCakes(ActionEvent event) {
		name_tf.setText("");
		flavour_tf.setText("");
		filling_tf.setText("");
		nbLayers_tf.setText("0");
		icing_tf.setText("");
		id_tf.setText("");
		glutenFree_tf.setSelected(false);
	}

	@FXML public void addCake(ActionEvent event)  {
		ErrorCake_tf.setText("");
		String name = name_tf.getText();
		if (name.equals("")) {
			name_tf.setStyle("-fx-border-color: red;");
			return;
		}

		String flavour = flavour_tf.getText();
		if (flavour.equals("")) {
			flavour_tf.setStyle("-fx-border-color: red;");
			return;
		}

		String filling = filling_tf.getText();
		if (filling.equals("")) {
			filling_tf.setStyle("-fx-border-color: red;");
			return;
		}

		String nbLayers = nbLayers_tf.getText();
		if (nbLayers.equals("") || nbLayers.equals("0")) {
			nbLayers_tf.setStyle("-fx-border-color: red;");
			return;
		}

		String icing = icing_tf.getText();
		if (icing.equals("")) {
			icing_tf.setStyle("-fx-border-color: red;");
			return;
		}

		Boolean glutenFree = glutenFree_tf.selectedProperty().getValue();

		try{
			Cake cake = new Cake( name, flavour, filling, icing, Integer.parseInt(nbLayers), glutenFree);
			this.cakeController.add(cake);
			this.cake_list.add(cake);
		} catch(NumberFormatException n) {
			ErrorCake_tf.setText("You need to introduce a number!");
			System.err.println(" Error " + n.getMessage()); System.out.println();
		} catch (AlreadyInRepoException a){
			ErrorCake_tf.setText(" Error " +a.getMessage());
			System.err.println(" Error " +a.getMessage()); System.out.println();
		} catch (NotFoundException n) {
			ErrorCake_tf.setText("Aici nu are de ce să intre");
			System.err.println(" Error " + n.getMessage()); System.out.println();
		}
		catch (FailFileOperationException e)
		{
			ErrorCake_tf.setText("Error reading from file");
			System.err.println( e.getMessage());
			System.out.println();
		}
		System.out.println(this.cakeController.findAll().toString());
		this.resetCakes(event);
	}

	@FXML public void updateCake(ActionEvent event)  {
		ErrorCake_tf.setText("");
		String name = name_tf.getText();
		if (name.equals("")) {
			name_tf.setStyle("-fx-border-color: red;");
			return;
		}
		else {
			name_tf.setStyle("-fx-border-color: none;");
		}

		String flavour = flavour_tf.getText();
		if (flavour.equals("")) {
			flavour_tf.setStyle("-fx-border-color: red;");
			return;
		}

		String filling = filling_tf.getText();
		if (filling.equals("")) {
			filling_tf.setStyle("-fx-border-color: red;");
			return;
		}

		String nbLayers = nbLayers_tf.getText();
		if (nbLayers.equals("") || nbLayers.equals("0")) {
			nbLayers_tf.setStyle("-fx-border-color: red;");
			return;
		}

		String icing = icing_tf.getText();
		if (icing.equals("")) {
			icing_tf.setStyle("-fx-border-color: red;");
			return;
		}

		Boolean glutenFree = glutenFree_tf.selectedProperty().getValue();

		String id = id_tf.getText();
		if (id.equals("")) {
			id_tf.setStyle("-fx-border-color: red;");
			return;
		}
		try {
			this.cakeController.update(new Cake(Integer.parseInt(id),name, flavour, filling, icing, Integer.parseInt(nbLayers), glutenFree), Integer.parseInt(id));
			this.cake_list.set(Integer.parseInt(id), new Cake(Integer.parseInt(id),name, flavour, filling, icing, Integer.parseInt(nbLayers), glutenFree));
		}catch(NumberFormatException n) {
			ErrorCake_tf.setText("You need to introduce a number!");
			System.err.println(" Error " + n.getMessage()); System.out.println();
		} catch (AlreadyInRepoException a){
			ErrorCake_tf.setText(" Error " +a.getMessage());
			System.err.println(" Error " +a.getMessage()); System.out.println();
		} catch (NotFoundException n) {
			ErrorCake_tf.setText("No element with that id");
			System.err.println(" Error " + n.getMessage()); System.out.println();
		}
		catch (FailFileOperationException e)
		{
			ErrorCake_tf.setText("Error writing to file:");
			System.err.println( e.getMessage()); System.out.println();
		}
		System.out.println(this.cakeController.findAll().toString());
		this.resetCakes(event);
	}

	@FXML public void deleteCake(ActionEvent event)  {
		ErrorCake_tf.setText("");
		String id = id_tf.getText();
		if (id.equals("")) {
			id_tf.setStyle("-fx-border-color: red;");
			return;
		}
		try {
			for(PlacedOrder o : this.ordersController.findAll())
				if(o.getCakeId().equals(Integer.parseInt(id)))
				{
					ErrorCake_tf.setText("You cannot delete a cake that has an order placed for it!");
					return;
				}
			this.cakeController.deleteById(Integer.parseInt(id));
			this.cake_list.remove(Integer.parseInt(id));
		}catch(NumberFormatException n) {
			ErrorCake_tf.setText("You need to introduce a number!");
			System.err.println(" Error " + n.getMessage()); System.out.println();
		} catch (NotFoundException n) {
			ErrorCake_tf.setText("No element with that id");
			System.err.println(" Error " + n.getMessage()); System.out.println();
		}
		catch (FailFileOperationException e)
		{
			ErrorCake_tf.setText("Error writing to file:");
			System.err.println(e.getMessage()); System.out.println();
		}
		System.out.println(this.cakeController.findAll().toString());
		this.resetCakes(event);
	}


	// the fields from PLACE ORDER part
	@FXML private TextField clientName_tf;
	@FXML private TextField address_tf;
	@FXML private DatePicker datepicker_tf;
	@FXML private TextField cake_id_tf;
	@FXML private TextField decorations_tf;
	@FXML private TextField idOrder_tf;


	//the error text:
	@FXML private Text ErrorOrder_tf;

	public void resetOrders(ActionEvent event) {
		clientName_tf.setText("");
		address_tf.setText("");
		datepicker_tf.setValue(null);
		cake_id_tf.setText("");
		decorations_tf.setText("");
	}

	//the fields from ORDERS VIEW
	@FXML private TableView<PlacedOrder> OrderView;
	@FXML private TableColumn<PlacedOrder,Integer> orderIDcol;
	@FXML private TableColumn<PlacedOrder,String> orderNAMEcol;
	@FXML private TableColumn<PlacedOrder,String> orderADDRESScol;
	@FXML private TableColumn<PlacedOrder,String> orderDATEcol;
	@FXML private TableColumn<PlacedOrder,Integer> orderCAKEIDcol;
	@FXML private TableColumn<PlacedOrder,String> orderDECORATIONScol;
	@FXML private TableColumn<PlacedOrder,String> orderFINISHEDScol;

	//the fields from ORDERS VIEW in REPORTS
	@FXML private TableView<PlacedOrder> ROrderView;
	@FXML private TableColumn<PlacedOrder,Integer> RorderIDcol;
	@FXML private TableColumn<PlacedOrder,String> RorderNAMEcol;
	@FXML private TableColumn<PlacedOrder,String> RorderADDRESScol;
	@FXML private TableColumn<PlacedOrder,String> RorderDATEcol;
	@FXML private TableColumn<PlacedOrder,Integer> RorderCAKEIDcol;
	@FXML private TableColumn<PlacedOrder,String> RorderDECORATIONScol;
	@FXML private TableColumn<PlacedOrder,String> RorderFINISHEDScol;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		cakeIDcol.setCellValueFactory(new PropertyValueFactory<>("id"));
		cakeNAMEcol.setCellValueFactory(new PropertyValueFactory<>("name"));
		cakeFLAVOURcol.setCellValueFactory(new PropertyValueFactory<>("flavor"));
		cakeFILLINGcol.setCellValueFactory(new PropertyValueFactory<>("filling"));
		cakeLAYERScol.setCellValueFactory(new PropertyValueFactory<>("number_of_layers"));
		cakeICINGcol.setCellValueFactory(new PropertyValueFactory<>("icing"));
		cakeGFreecol.setCellValueFactory(new PropertyValueFactory<>("glutenFree"));

		for(Cake elem: cakeController.findAll()) {
			cake_list.add(elem);
		}
		//System.out.println(CakeTableView);
		System.out.println(cake_list);

		CakeTableView.setItems(cake_list);

		cakeIDcol2.setCellValueFactory(new PropertyValueFactory<>("id"));
		cakeNAMEcol2.setCellValueFactory(new PropertyValueFactory<>("name"));
		cakeFLAVOURcol2.setCellValueFactory(new PropertyValueFactory<>("flavor"));
		cakeFILLINGcol2.setCellValueFactory(new PropertyValueFactory<>("filling"));
		cakeLAYERScol2.setCellValueFactory(new PropertyValueFactory<>("number_of_layers"));
		cakeICINGcol2.setCellValueFactory(new PropertyValueFactory<>("icing"));
		cakeGFreecol2.setCellValueFactory(new PropertyValueFactory<>("glutenFree"));

		CakeTableView2.setItems(cake_list);


		orderIDcol.setCellValueFactory(new PropertyValueFactory<>("id"));
		orderNAMEcol.setCellValueFactory(new PropertyValueFactory<>("clientName"));
		orderADDRESScol.setCellValueFactory(new PropertyValueFactory<>("clientAddress"));
		orderDATEcol.setCellValueFactory(new PropertyValueFactory<>("dueDate"));
		orderCAKEIDcol.setCellValueFactory(new PropertyValueFactory<>("CakeNameAndID"));
		orderDECORATIONScol.setCellValueFactory(new PropertyValueFactory<>("decorations"));
		orderFINISHEDScol.setCellValueFactory(new PropertyValueFactory<>("isFinished"));

		for(PlacedOrder elem: ordersController.findAll()) {
			orders_list.add(elem);
		}
		System.out.println(OrderView);
		System.out.println(orders_list);

		OrderView.setItems(orders_list);

		RorderIDcol.setCellValueFactory(new PropertyValueFactory<>("id"));
		RorderNAMEcol.setCellValueFactory(new PropertyValueFactory<>("clientName"));
		RorderADDRESScol.setCellValueFactory(new PropertyValueFactory<>("clientAddress"));
		RorderDATEcol.setCellValueFactory(new PropertyValueFactory<>("dueDate"));
		RorderCAKEIDcol.setCellValueFactory(new PropertyValueFactory<>("CakeNameAndID"));
		RorderDECORATIONScol.setCellValueFactory(new PropertyValueFactory<>("decorations"));
		RorderFINISHEDScol.setCellValueFactory(new PropertyValueFactory<>("isFinished"));
		ROrderView.setItems(FilteredOrders_list);

		RcakeIDcol.setCellValueFactory(new PropertyValueFactory<>("id"));
		RcakeNAMEcol.setCellValueFactory(new PropertyValueFactory<>("name"));
		RcakeFLAVOURcol.setCellValueFactory(new PropertyValueFactory<>("flavor"));
		RcakeFILLINGcol.setCellValueFactory(new PropertyValueFactory<>("filling"));
		RcakeLAYERScol.setCellValueFactory(new PropertyValueFactory<>("number_of_layers"));
		RcakeICINGcol.setCellValueFactory(new PropertyValueFactory<>("icing"));
		RcakeGFreecol.setCellValueFactory(new PropertyValueFactory<>("glutenFree"));
		ReportCakeTableView.setItems(FilteredCakes_list);


	}

	public void PlaceOrder(ActionEvent event) {
		ErrorOrder_tf.setText("");
		String clientName = clientName_tf.getText();
		if (clientName.equals("")) {
			clientName_tf.setStyle("-fx-border-color: red;");
			return;
		}
		else {
			clientName_tf.setStyle("-fx-border-color: none;");
		}

		String address = address_tf.getText();
		if (address.equals("")) {
			address_tf.setStyle("-fx-border-color: red;");
			return;
		}

		LocalDate date = datepicker_tf.getValue();
		if (date==null) {
			datepicker_tf.setStyle("-fx-border-color: red;");
			return;
		}

		String cakeId = cake_id_tf.getText();
		if (cakeId.equals("")) {
			cake_id_tf.setStyle("-fx-border-color: red;");
			return;
		}

		String decorations = decorations_tf.getText();
		if (decorations.equals("")) {
			decorations_tf.setStyle("-fx-border-color: red;");
			return;
		}

		try{
			PlacedOrder order = new PlacedOrder( clientName, address, date.toString(), cakeController.findById(Integer.parseInt(cakeId)), decorations);
			this.ordersController.add(order);
			this.orders_list.add(order);
		} catch (AlreadyInRepoException | NotFoundException a){
			ErrorOrder_tf.setText(" Error " +a.getMessage());
			System.err.println(" Error " +a.getMessage()); System.out.println();
		} catch(NumberFormatException n) {
			ErrorOrder_tf.setText("The id needs to be a number!");
			System.err.println(" Error " + n.getMessage()); System.out.println();
		}
		catch (FailFileOperationException e)
		{
			ErrorCake_tf.setText("Error reading from file");
			System.err.println( e.getMessage());
			System.out.println();
		}
		System.out.println(this.ordersController.findAll().toString());
		this.resetOrders(event);
	}

	public void UpdateOrder(ActionEvent event) {
		ErrorOrder_tf.setText("");
		String clientName = clientName_tf.getText();
		if (clientName.equals("")) {
			clientName_tf.setStyle("-fx-border-color: red;");
			return;
		}
		else {
			clientName_tf.setStyle("-fx-border-color: none;");
		}

		String address = address_tf.getText();
		if (address.equals("")) {
			address_tf.setStyle("-fx-border-color: red;");
			return;
		}

		LocalDate date = datepicker_tf.getValue();
		if (date==null) {
			datepicker_tf.setStyle("-fx-border-color: red;");
			return;
		}

		String cakeId = cake_id_tf.getText();
		if (cakeId.equals("")) {
			cake_id_tf.setStyle("-fx-border-color: red;");
			return;
		}

		String decorations = decorations_tf.getText();
		if (decorations.equals("")) {
			decorations_tf.setStyle("-fx-border-color: red;");
			return;
		}

		String id = idOrder_tf.getText();
		if (id.equals("")) {
			idOrder_tf.setStyle("-fx-border-color: red;");
			return;
		}
		try{
			this.ordersController.update(new PlacedOrder(Integer.parseInt(id),clientName, address, date.toString(), cakeController.findById(Integer.parseInt(cakeId)), decorations), Integer.parseInt(id));
			this.orders_list.set(Integer.parseInt(id),new PlacedOrder(Integer.parseInt(id),clientName, address, date.toString(), cakeController.findById(Integer.parseInt(cakeId)), decorations));

		} catch (AlreadyInRepoException | NotFoundException a){
			ErrorOrder_tf.setText(" Error " +a.getMessage());
			System.err.println(" Error " +a.getMessage()); System.out.println();
		} catch(NumberFormatException n) {
			ErrorOrder_tf.setText("The id needs to be a number!");
			System.err.println(" Error " + n.getMessage());
			System.out.println();
		} catch (FailFileOperationException e) {
			ErrorOrder_tf.setText("Error writing to file:");
			System.err.println( e.getMessage()); System.out.println();
		}
		System.out.println(this.ordersController.findAll().toString());
		this.resetOrders(event);
	}

	public void DeleteOrder(ActionEvent event) {
		ErrorOrder_tf.setText("");
 	    String id = idOrder_tf.getText();
		if (id.equals("")) {
			idOrder_tf.setStyle("-fx-border-color: red;");
			return;
		}
		try{
			this.ordersController.deleteById(Integer.parseInt(id));
			this.orders_list.remove(Integer.parseInt(id));
		} catch ( NotFoundException a){
			ErrorOrder_tf.setText(" Error " +a.getMessage());
			System.err.println(" Error " +a.getMessage()); System.out.println();
		} catch(NumberFormatException n) {
			ErrorOrder_tf.setText("The id needs to be a number!");
			System.err.println(" Error " + n.getMessage());
			System.out.println();
		} catch (FailFileOperationException e) {
			ErrorOrder_tf.setText("Error writing to file:");
			System.err.println( e.getMessage()); System.out.println();
		}
		System.out.println(this.ordersController.findAll().toString());
		this.resetOrders(event);
	}

	// the fields from FINISH ORDER part
	@FXML private PasswordField password_tf;
	@FXML private TextField orderId_tf;

	//the error text:
	@FXML private Text ErrorUserUndefined_tf;

	public void resetFinish(ActionEvent event) {
		password_tf.setText("");
		orderId_tf.setText("");
	}

	public void FinishOrder(ActionEvent event) {
		ErrorUserUndefined_tf.setText("");
		String password = password_tf.getText();
		if (!password.equals("1234")&&!password.equals("0000")&&!password.equals("9a83")) {
			clientName_tf.setStyle("-fx-border-color: red;");
			ErrorUserUndefined_tf.setText("The credentials are incorrect!");
			return;
		}

		String orderId = orderId_tf.getText();
		if (orderId.equals("")) {
			orderId_tf.setStyle("-fx-border-color: red;");
			return;
		}

		try{
			this.ordersController.FinishOrder(Integer.parseInt(orderId));
			this.orders_list.set(Integer.parseInt(orderId),this.ordersController.findById(Integer.parseInt(orderId)));

			System.out.println(this.ordersController.findById(Integer.parseInt(orderId)).toString());

		} catch (NotFoundException n) {
			ErrorUserUndefined_tf.setText(" Error " + n.getMessage());
			System.err.println(" Error " + n.getMessage()); System.out.println();
		} catch(NumberFormatException n) {
			ErrorUserUndefined_tf.setText("The id needs to be a number!");
			System.err.println(" Error " + n.getMessage()); System.out.println();
		}
		this.resetFinish(event);
	}


	public void BasicReportsOrders(ActionEvent event) {
		this.FilteredOrders_list.clear();
		this.FilteredOrders_list.addAll(orders_list);
	}


	public void OrdersForCakeOfId(ActionEvent event) {
		ReportsError_tf.setText("");
		this.FilteredOrders_list.clear();
		String id = ReportCake_id_tf.getText();
		if (id.equals("")) {
			ReportCake_id_tf.setStyle("-fx-border-color: red;");
			return;
		}
		try{
			FilteredOrders_list.addAll(this.ordersController.streamOrdersForCake(Integer.parseInt(id)));
			if(FilteredOrders_list.isEmpty())
			{
				ReportsError_tf.setText("There are no orders for the cake of id:" + id);
			}
		} catch(NumberFormatException n) {
			ReportsError_tf.setText("The id needs to be a number!");
			System.err.println(" Error " + n.getMessage());
			System.out.println();
		}
		System.out.println(this.FilteredOrders_list);
		ReportCake_id_tf.setText("");
	}

	public void FinishedOrders(ActionEvent event) {
		ReportsError_tf.setText("");
		this.FilteredOrders_list.clear();
		FilteredOrders_list.addAll(this.ordersController.streamFinishedOrders());
		if(FilteredOrders_list.isEmpty())
		{
			ReportsError_tf.setText("All the orders are finished");
		}
		System.out.println(this.FilteredOrders_list);
	}

	public void NotFinishedOrders(ActionEvent event) {
		ReportsError_tf.setText("");
		this.FilteredOrders_list.clear();
		FilteredOrders_list.addAll(this.ordersController.streamNotFinishedOrders());
		if(FilteredOrders_list.isEmpty())
		{
			ReportsError_tf.setText("There are no finished orders");
		}
		System.out.println(this.FilteredOrders_list);
	}

	public void BasicReportsCakes(ActionEvent event) {
		this.FilteredCakes_list.clear();
		this.FilteredCakes_list.addAll(cake_list);
	}

	public void CakesForDate(ActionEvent event) {
		ReportsError_tf.setText("");
		this.FilteredCakes_list.clear();
		LocalDate date = ReportDate_id_tf.getValue();
		if (date==null) {
			ReportDate_id_tf.setStyle("-fx-border-color: red;");
			return;
		}
		try{
			FilteredCakes_list.addAll(this.ordersController.streamCakesForDate(date.toString()));
			if(FilteredCakes_list.isEmpty())
			{
				ReportsError_tf.setText("There are no cakes for the date: " + date);
			}
		} catch(NumberFormatException n) {
			ReportsError_tf.setText("The id needs to be a number!");
			System.err.println(" Error " + n.getMessage());
			System.out.println();
		}
		System.out.println(this.FilteredCakes_list);
		ReportDate_id_tf.setValue(null);
	}

	public void GlutenFreeCakes(ActionEvent event) {
		ReportsError_tf.setText("");
		this.FilteredCakes_list.clear();

		FilteredCakes_list.addAll(this.cakeController.streamGlutenFreeCakes());
			if(FilteredCakes_list.isEmpty())
			{
				ReportsError_tf.setText("There are no GLUTEN FREE cakes " );
			}

		System.out.println(this.FilteredCakes_list);
	}

	public void GlutenCakes(ActionEvent event) {
		ReportsError_tf.setText("");
		this.FilteredCakes_list.clear();

		FilteredCakes_list.addAll(this.cakeController.streamNotGlutenFreeCakes());
		if(FilteredCakes_list.isEmpty())
		{
			ReportsError_tf.setText("There are no cakes with gluten" );
		}

		System.out.println(this.FilteredCakes_list);
	}


}
